package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/dev-area/horse-exchange/entity"
)

type getBreedsResponse struct {
	Data []entity.Breed `json:"data"`
}

func (h *Handler) getBreeds(c *gin.Context) {
	breads, err := h.service.Breed.GetBreeds()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, viper.GetString("messages.errors.retry_later"))
		return
	}

	c.JSON(http.StatusOK, getBreedsResponse{
		Data: breads,
	})
}
