package service

import (
	"gitlab.com/dev-area/horse-exchange/entity"
	"gitlab.com/dev-area/horse-exchange/pkg/repository"
)

type Task interface {
	GetTasks() ([]entity.Task, error)
}

type TaskService struct {
	repos repository.Task
}

func NewTaskService(r repository.Task) *TaskService {
	return &TaskService{repos: r}
}

func (b *TaskService) GetTasks() ([]entity.Task, error) {
	return b.repos.GetTasks()
}
