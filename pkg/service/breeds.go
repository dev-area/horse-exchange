package service

import (
	"gitlab.com/dev-area/horse-exchange/entity"
	"gitlab.com/dev-area/horse-exchange/pkg/repository"
)

type Breed interface {
	GetBreeds() ([]entity.Breed, error)
}

type BreedService struct {
	repos repository.Breed
}

func NewBreedService(r repository.Breed) *BreedService {
	return &BreedService{repos: r}
}

func (b *BreedService) GetBreeds() ([]entity.Breed, error) {
	return b.repos.GetBreeds()
}
