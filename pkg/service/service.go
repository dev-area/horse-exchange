package service

import (
	"gitlab.com/dev-area/horse-exchange/pkg/repository"
)

type Service struct {
	Horse
	Breed
	Task
}

func NewService(r *repository.Repository) *Service {
	return &Service{
		Horse: NewHorseService(r.Horse),
		Breed: NewBreedService(r.Breed),
		Task:  NewTaskService(r.Task),
	}
}
